﻿using System;

namespace Shop
{
    class Program
    {
        static void Main(string[] args)
        {
            Shop shop = new Shop("Магазин");
            shop.AddItems(new Meat());
            shop.AddItems(new Fish());
            shop.AddItems(new Сucumbers());
            shop.AddItems(new Tomatoes());
            shop.AddItems(new Sofa());
            shop.AddItems(new Table());

            foreach (Items item in shop.ItemsList)
            {
                Console.WriteLine(item.Information);
                item.ShowItemsInfo();
            }

            Console.WriteLine("Введите номер продукта, которые хотите удалить.");
            string _producttoremove = Console.ReadLine();
            shop.RemoveItems(_producttoremove);


            Console.WriteLine("Список сьедобных продуктов:");
            for (int i = 0; i < shop.ItemsList.Count; i++)
            {
                if (shop.ItemsList[i] is Eatable)
                {
                    Console.WriteLine(shop.ItemsList[i] as Eatable);
                }
            }

            Console.WriteLine("Список не сьедобных продуктов:");
            for (int i = 0; i < shop.ItemsList.Count; i++)
            {
                if (shop.ItemsList[i] is UnEatable)
                {
                    Console.WriteLine(shop.ItemsList[i] as UnEatable);
                }
            }

            Console.WriteLine("Список вегетарианских продуктов:");
            for (int i = 0; i < shop.ItemsList.Count; i++)
            {
                if (shop.ItemsList[i] is IVegetarian)
                {
                    Console.WriteLine(shop.ItemsList[i] as IVegetarian);
                }
            }
        }
    }
}
