﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    abstract class Items
    {
        protected string _information;
        protected string _name;
        protected float _price;
        protected float _amount;

        public string Information => _information;
        public string Name => _name;
        public float Price => _price;
        public float Amount => _amount;

        public Items()
        {
            _information = "Иформация:";
            _name = "Something";
            _price = 10f;
            _amount = 10f;
        }

        public virtual void ShowItemsInfo()
        {
            Console.WriteLine("__________________");
            Console.WriteLine($"Наименование: {Name} Цена: {Price} Количество: {Amount}");
        }

        public override string ToString()
        {
            return $"Продукт: {Name} Цена: {Price} Количество: {Amount} ";
        }
    }
}
