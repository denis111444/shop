﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    class Shop
    {
        protected string _shopname;
        public List<Items> ItemsList = new List<Items>();

        public Shop(string _shopname)
        {
            this._shopname = _shopname;
        }

        public string ShopName
        {
            get { return _shopname; }
            set
            {
                if (value != null)
                {
                    value = ShopName;
                }
                else
                {
                    Console.WriteLine("Введите название магазина.");
                }
            }
        }

        public void AddItems(Items item)
        {
            Console.WriteLine("Введите количество продуктов.");
            int count = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < count; i++)
            {
                ItemsList.Add(item);
            }
        }

        public void RemoveItems(string name)
        {
            foreach (Items items in ItemsList)
            {
                if (items.Name == name)
                {
                    ItemsList.Remove(items);
                }
            }
        }

        public void SortItems()
        {
            Items temp;
            for (int i = 0; i < ItemsList.Count; i++)
            {
                for (int j = i + 1; j < ItemsList.Count; j++)
                {
                    if (ItemsList[i].Price > ItemsList[j].Price)
                    {
                        temp = ItemsList[i];
                        ItemsList[i] = ItemsList[j];
                        ItemsList[j] = temp;
                    }
                }
            }

        }

        public void CheckVegetarian()
        {
            for (int i = 0; i < ItemsList.Count; i++)
            {
                if (ItemsList[i] is IVegetarian)
                {
                    Console.WriteLine(ItemsList[i] as IVegetarian);
                }
            }
        }
    }
}
