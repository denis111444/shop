﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    abstract class Eatable : Items
    {
        protected string _manufactureddate;
        protected string _expirationdate;

        public string Manufactureddate => _manufactureddate;
        public string Expirationdate => _expirationdate;

        public Eatable()
        {
            _manufactureddate = "15-04-22";
            _expirationdate = "7 суток";
        }

        public override void ShowItemsInfo()
        {
            base.ShowItemsInfo();
            Console.WriteLine($"Дата изготовления: {_manufactureddate}  \nСрок реализации: {_expirationdate}");
        }
    }
}
