﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    class Sofa: UnEatable
    {
        protected string _dimensions;
        protected string _upholstery;
        public string Dimensions
        {
            get { return _dimensions; }
        }
        public string Upholstery
        {
            get { return _upholstery; }
        }

        public Sofa()
        {
            _name = "Диван";
            _price = 1230f;
            _amount = 1;
            _dimensions = "Ширина 85см, Длина 210см, Высота 90см";
            _upholstery = "Кожа";
        }

        public override void ShowItemsInfo()
        {
            base.ShowItemsInfo();
            Console.WriteLine($"Габаритные размеры: {Dimensions}\nВес: {Upholstery}");
        }
    }
}
