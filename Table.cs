﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    class Table : UnEatable
    {
        protected string _dimensions;
        protected float _weight;
        public string Dimensions
        {
            get { return _dimensions; }
        }
        public float Weight
        {
            get { return _weight; }
        }

        public Table()
        {
            _name = "Cтол кофейный";
            _price = 115f;
            _amount = 1;
            _dimensions = "Ширина 60см, Длина 60см, Высота 42см";
            _weight = 14f;
        }

        public override void ShowItemsInfo()
        {
            base.ShowItemsInfo();
            Console.WriteLine($"Габаритные размеры: {Dimensions}\nВес: {Weight}");
        }
    }
}
