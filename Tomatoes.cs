﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Shop
{
    class Tomatoes : Eatable, IVegetarian
    {
        public Tomatoes()
        {
            _name = "Помидорчики";
            _price = 9f;
            _amount = 1;
            _manufactureddate = "08-04-22";
            _expirationdate = "16 суток";
        }
    }
}
